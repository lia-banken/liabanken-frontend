import React, {useEffect, useState} from 'react';
import {Button, makeStyles} from '@material-ui/core';
import SearchBar from "./SearchBar";
import Dropdown from "./Dropdown";


const useStyles = makeStyles(() => ({
    list: {
        display: 'flex',
        flexDirection: "column",
        justifyContent: 'center',
        alignItems: 'center',
        padding: '30px'

    },
    item: {
        margin: '10px',
        alignItems: "center",
        justifyItems: "center",
        textAlign: "center",
        padding:"10px",
        width:"300px",
        backgroundColor:"#D3F5F3"
    },
}))

interface ListData {
    list: {
        id: string,
        name: string,
        ort: string
    }[],
    city: string,
    searchText: string
}

const ListItems: React.FC<ListData> = ({list, city, searchText}) => {
    const classes = useStyles();
    const [filtredList, setFiltredList] = useState([{id: '1', name: 'school 1', ort: 'stockholm'}]);

    useEffect(() => {
        filterSchoolList(city);
    },[searchText, city]);

    const filterSchoolList = (ort: string) => {
        let temporaryArray: { id: string, name: string, ort: string }[];
        if (ort === "" && searchText.length === 0) {
            setFiltredList(list)
        } else {
            if (searchText.length > 0 && ort === "") {
                temporaryArray = list.filter(value => value.name.toLowerCase().includes(searchText.toLowerCase()) ||
                    !searchText.toLowerCase())
                setFiltredList(temporaryArray);
            } else {
                temporaryArray = list.filter(value => value.ort === ort)
                setFiltredList(temporaryArray);
            }
        }
    }

    return (
        <div className={classes.list}>
            {filtredList.map((item) => (<div className={classes.item}>
                    <Button key={item.id}>{item.name}</Button>
                    <p>{item.ort}</p>
                </div>
            ))}
        </div>
    );
};

export default ListItems;