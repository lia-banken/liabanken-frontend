import React, {useEffect} from 'react';
import {
    alpha, createStyles,
    FormControl,
    InputBase,
    InputLabel,
    makeStyles,
    MenuItem,
    Select,
    Theme,
    withStyles
} from "@material-ui/core";
import WorkIcon from '@material-ui/icons/Work';
import SchoolIcon from '@mui/icons-material/School';
import SearchIcon from '@material-ui/icons/Search';

const useStyles = makeStyles((theme) => ({
    iconContainer: {
        display: 'flex',
        flexDirection: 'row',
        marginTop: 30,
        alignItems: 'center',
    },
    search: {
        position: 'relative',
        border: '2px solid black',
        borderRadius: '19px',
        backgroundColor: alpha(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: alpha(theme.palette.common.white, 0.25),
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        marginTop: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: '300px',
        },
    },

    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 0),
        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: '20ch',
        },
    },
    searchIcon: {
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    iconSuit: {
        marginRight: '-10px'
    },
}))

// interface placeHolderName {
//     name?: string,
// }

interface Search {
    updateSearchText: (arg: string) => any
}

const SearchBar: React.FC<Search> = ({updateSearchText}) => {
    const classes = useStyles();

    function handleSearchTextChange(e: React.ChangeEvent<HTMLInputElement>) {
        const {value} = e.target
        updateSearchText(value)
    }

    return (
        <div className={classes.iconContainer}>
            <div className={classes.iconSuit}>
                <WorkIcon/>
            </div>
            <div className={classes.search}>
                <div className={classes.searchIcon}>
                    <SearchIcon/>
                </div>
                <InputBase
                    placeholder="Search"
                    classes={{
                        root: classes.inputRoot,
                        input: classes.inputInput,
                    }}
                    inputProps={{'aria-label': 'search'}}
                    onChange={handleSearchTextChange}
                />
            </div>
        </div>
    );
};

export default SearchBar;
