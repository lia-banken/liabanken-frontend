import React from 'react';
import {makeStyles} from '@material-ui/core';
import {IconButton,} from "@mui/material";
import DeleteIcon from "@material-ui/icons/Delete";


const useStyles = makeStyles((theme) => ({
    list: {
        display: "flex",
        margin: "20px",
        padding: "10px",
        width: "300px",
        justifyContent: "space-around",
        backgroundColor: "#D3F5F3"
    },
    listItem: {
        marginRight: "0px"
    }
}));

export interface IDataList {
    name: string,
    lastname: string,
    email: string,
    password?: string
}


interface IListInList {
    list: IDataList[]

    remove(delet: string): void
}

const ReusableList: React.FC<IListInList> = ({list, remove}) => {
    const classes = useStyles();

    return (
        <div>
            {list.map((e, id) => {
                return (
                    <div className={classes.list} key={id}>

                        <p className={classes.listItem}>{e.name}</p>
                        <p className={classes.listItem}>{e.lastname}</p>
                        <div className={classes.listItem}>
                            <IconButton aria-label="delete"
                                        onClick={() => remove(e.email)}
                            >
                                <DeleteIcon/>
                            </IconButton>
                        </div>

                    </div>

                )
            })}
        </div>
    );
};

export default ReusableList;
