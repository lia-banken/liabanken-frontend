import React, {useState} from 'react';
import {makeStyles, Theme, createStyles} from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import AddCircleIcon from '@material-ui/icons/AddCircle';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        list: {
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            justifyItems: "center",
            alignItems: "center",
        },
        items: {
            display: "flex",
            flexDirection: "row",
            paddingTop: "25px",
            width: "400px",
            justifyContent: "center",
            margin:"10px",
            padding:"10px",
            backgroundColor:"#D3F5F3"
        },
        text: {
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            textAlign: "center",
            justifyContent: "center",
        },
        icons: {
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",

        }
    }),
);

interface NameOfSchool {
    name?: string
}

const QueryList = (props: NameOfSchool) => {
    const classes = useStyles();
    const [dense, setDense] = React.useState(false);
    const [secondary, setSecondary] = React.useState(false);
    const [schoolQueryList, setschoolQueryList] = useState([
        {id: '1', name: 'Lunds universitet ', ort: 'Lunds'},
        {id: '2', name: 'Göteborgs universitet', ort: 'Göteborgs'},
        {id: '3', name: 'Linköpings universitet', ort: 'linköpings'},
        {id: '4', name: 'Karolinska institutet ', ort: 'stockholm'},
    ]);
    return (
        <div>
            <List className={classes.list} dense={dense}>
                {
                    schoolQueryList.map(school => (
                        <div className={classes.items}>
                            <div className={classes.text}>
                                {school.name}
                            </div>
                            <div className={classes.icons}>
                                <IconButton aria-label="add">
                                    <AddCircleIcon/>
                                </IconButton>
                                <IconButton aria-label="delete">
                                    <DeleteIcon/>
                                </IconButton>
                            </div>
                        </div>
                    ))
                }
            </List>
        </div>
    );
};

export default QueryList;