import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Paper, {PaperProps} from '@mui/material/Paper';

import {TextField} from "@mui/material";
import {makeStyles} from "@material-ui/core/styles";
import AddIcon from "@material-ui/icons/AddCircleOutline";
import {FormEvent} from "react";


const useStyles = makeStyles((theme) => ({
    dialog: {
        width: "100%"
    },
    dialogContext: {
        display: "flex",
        flexDirection: "column",
        padding: "10px"
    },
    textField: {}
}))

export interface IPropsForm {

    onAdd(name:string, lastName:string,email:string,password?:string):void

}

export const AdminCreateAccountModal:React.FC<IPropsForm> = ({onAdd}) => {
    const classes = useStyles()
    const [open, setOpen] = React.useState(false);
    const [name, setName] = React.useState("");
    const [lastName, setLastName] = React.useState("");
    const [email, setEmail] = React.useState("");
    const [password, setPassword] = React.useState("");

 /*   const dataInfo = () => {
        const data = {
            name: name,
            lastName: name,
            emali: email,
            password: password
        }
        return data

    };*/
    const dataInfo = (e:FormEvent<HTMLFormElement>): void => {
        e.preventDefault()
        onAdd(name,lastName,email,password)
        setOpen(false)
        console.log(onAdd)
    }

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);

    };

    return (
        <div>
            <Button variant="contained" endIcon={<AddIcon/>} onClick={handleClickOpen}>
                Add admin
            </Button>

            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="draggable-dialog-title"

            >
                <form onSubmit={dataInfo}>
                <DialogContent>
                    <TextField id="outlined-basic"
                               label="Name"
                               variant="outlined"
                               type='text'
                               autoFocus
                               margin="dense"
                               fullWidth
                               onChange={(e) => setName(e.target.value)}
                    />
                    <TextField id="outlined-basic"
                               label="LastName"
                               variant="outlined"
                               type='text'
                               autoFocus
                               margin="dense"
                               fullWidth
                               onChange={(e) => setLastName(e.target.value)}
                    />
                    <TextField id="outlined-basic"
                               label="E-mail"
                               variant="outlined"
                               type='text'
                               autoFocus
                               margin="dense"
                               fullWidth
                               onChange={(e) => setEmail(e.target.value)}
                    />
                    <TextField id="outlined-basic"
                               label="Password"
                               variant="outlined"
                               type='password'
                               autoFocus
                               margin="dense"
                               fullWidth

                               onChange={(e) => setPassword(e.target.value)}
                    />
                </DialogContent>

                <DialogActions>
                    <Button autoFocus onClick={handleClose}>
                        Cancel
                    </Button>
                    <Button type='submit' onClick={handleClose}>Save</Button>

                </DialogActions>
                </form>
            </Dialog>
        </div>
    );
}
