import React from 'react';
import {Button, makeStyles} from "@material-ui/core";
import Link from '@material-ui/core/Link';
import LocalPhoneIcon from '@material-ui/icons/LocalPhone';

const useStyles = makeStyles(() => ({
    container: {

        width: "100%",
        height: "5%px",
        position:"fixed",
        bottom:"0px",
        // marginBottom:"5px",
        background: "whitesmoke",
        display: "flex",
        flexWrap:"wrap",
        justifyContent: "space-around",
        border: "1px solid #83e6d4",
        backgroundColor: "#e6e6ff",
        boxShadow: "5px 5px 2px #404040",
        marginLeft:""
    },
    link: {
        color: "black",
        fontSize: "15px",
        textAlign: "center",
        padding:"10px"

    },
    links: {
        display: "flex",
        flexDirection:"row",
        flexWrap:"wrap",
        paddingLeft:"30px",
        alignContent: "center"
    },
    button: {
        backgroundColor: "#83e6d4",
        color:"white",
        textShadow: "1px 1px 2px black, 0 0 25px blue, 0 0 5px darkblue; ",

    },
    followUs:{
        display:"flex",
        flexWrap:"wrap",
        alignItems: "center",
        justifyContent: "space-evenly",

    }
}))

const Footer = () => {
    return (
        <div className={useStyles().container}>

            <Button
                variant="contained"
                className={useStyles().button}
                startIcon={<LocalPhoneIcon/>}
            >Kontakta oss</Button>

            <div className={useStyles().followUs}>
                <p >Följ oss på</p>
                <div className={useStyles().links}>
                    <Link href="#" color="inherit" className={useStyles().link}>
                        Facebook
                    </Link>
                    <Link href="#" color="inherit" className={useStyles().link}>
                        LinkedIn
                    </Link>
                    <Link href="#" color="inherit" className={useStyles().link}>
                        Youtube
                    </Link>
                    <Link href="#" color="inherit" className={useStyles().link}>
                        Instagram
                    </Link>
                </div>
            </div>


        </div>
    );
};

export default Footer;
