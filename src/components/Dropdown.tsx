import React from 'react';
import {
    alpha,
    createStyles,
    FormControl, InputBase,
    InputLabel,
    makeStyles,
    MenuItem,
    Select,
    Theme,
    withStyles
} from "@material-ui/core";

const BootstrapInput = withStyles((theme: Theme) =>
    createStyles({
        root: {
            'label + &': {
                marginTop: theme.spacing(3),
            },
        },
        input: {
            borderRadius: 4,
            position: 'relative',
            backgroundColor: theme.palette.background.paper,
            border: '1px solid #ced4da',
            fontSize: 16,
            padding: '10px 26px 10px 12px',
            transition: theme.transitions.create(['border-color', 'box-shadow']),
            fontFamily: [
                '-apple-system',
                'BlinkMacSystemFont',
                '"Segoe UI"',
                'Roboto',
                '"Helvetica Neue"',
                'Arial',
                'sans-serif',
                '"Apple Color Emoji"',
                '"Segoe UI Emoji"',
                '"Segoe UI Symbol"',
            ].join(','),
            '&:focus': {
                borderRadius: 4,
                borderColor: '#80bdff',
                boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
            },
        },
    }),
)(InputBase);

const useStyles = makeStyles((theme) => ({
    formControl: {
        display: 'flex',
        margin: theme.spacing(1),
        minWidth: 100,
        marginBottom: '30px',
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}))

interface Ort {
    updateCity: (arg:string) => any
}

const Dropdown:React.FC<Ort> = ({updateCity}) => {
    const classes = useStyles();
    const [stad, setStad] = React.useState('');
    const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setStad(event.target.value as string);
    };

    const chooseCityFromDropdown = (stad:string) => {
            updateCity(stad)
    };

    return (
        <div>
            <FormControl className={classes.formControl}>
                <InputLabel id="demo-customized-select-label">Ort</InputLabel>
                <Select
                    className={classes.selectEmpty}
                    labelId="demo-customized-select-label"
                    id="demo-customized-select"
                    value={stad}

                    onChange={handleChange}
                    input={<BootstrapInput />}
                >
                    <MenuItem onClick={() => chooseCityFromDropdown("")} value="">None</MenuItem>
                    <MenuItem onClick={() => chooseCityFromDropdown("stockholm")} value={10}>Stockholm</MenuItem>
                    <MenuItem onClick={() =>chooseCityFromDropdown("örebro")} value={20}>Örebro</MenuItem>
                    <MenuItem  onClick={() =>chooseCityFromDropdown("malmö")} value={30}>Malmö</MenuItem>
                </Select>
            </FormControl>
        </div>
    );
};

export default Dropdown;