import React, {useState} from 'react';
import {Backdrop, Button, Fade, makeStyles} from "@material-ui/core";
import Modal from "@material-ui/core/Modal";

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
    headerText: {
        textAlign: "center",
        paddingBottom:"10px"
    },
}))

const CreateAccountModal = () => {
    const [open, setOpen] = useState(false);

    const handleOpen = () => {
        setOpen(true);

    }
    const handleClose = () => {
        setOpen(false);
    }

    return (
        <div className={useStyles().headerText}>
            <h1>Start your journey with us </h1>

            <Button onClick={handleOpen} variant="outlined">Skapa Konto</Button>

            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={useStyles().modal}
                open={open}

                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className={useStyles().paper}>
                        <h2 id="transition-modal-title">Transition modal</h2>
                        <p id="transition-modal-description">react-transition-group animates me.</p>
                        <button type="button" onClick={handleClose}>
                            Create
                        </button>
                    </div>
                </Fade>
            </Modal>
        </div>
    );
};

export default CreateAccountModal;