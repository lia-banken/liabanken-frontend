import React, {useState} from 'react';
import {Button, makeStyles} from '@material-ui/core';

const useStyles = makeStyles(() => ({
    btn: {
        textTransform: "none"
    }
}));

interface BtnInterface {
    title: string,
}

const Btn: React.FC<BtnInterface> = (props) => {
    const classes = useStyles();
    const [text, setText] = useState();

   /* const toggelButton = (props: string) => {
        if (props == "props.title") {
            if (clickedOnList)
                setClickedOnList(false)
            setClickedOnQuery(true)
        } else if (props == "skolList") {
            if (clickedOnQuery)
                setClickedOnQuery(false)
            setClickedOnList(true)
        }
    }*/

    const btnHandler = () => {
        console.log(1)
    }
    return (
        <div>
            <Button onClick={() => btnHandler()} className={classes.btn}>{props.title}</Button>
        </div>
    );
};

export default Btn;