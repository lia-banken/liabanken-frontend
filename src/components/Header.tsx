import {makeStyles} from '@material-ui/core';
import React, {FC} from 'react';
import logo from '../assets/logo.png';
import NavigationBar from './NavigationBar';


const useStyles = makeStyles(() => ({
    img: {
        width: 200,
        height: 100
    },
    logo: {
        display: "flex",
        justifyContent: "center",
        flexDirection: "column",
        alignItems: "center",

    },
    logoName:{
        fontSize: "20px",

    }

}));


const Header: FC = () => {
const classes = useStyles();
    return (

        <div className={classes.logo}>
            <img src={logo} alt={""} className={classes.img}/>
            <p className={classes.logoName}>LiaBanken</p>
        </div>

    );
}

export default Header;
