import {Button, makeStyles} from '@material-ui/core';
import React, {FC} from 'react';
import {useHistory} from 'react-router-dom'

const useStyles = makeStyles(() => ({
        img: {
            width: 200,
            height: 100
        },
        navigationBar: {
            display: "flex",
            marginRight: "auto",
            marginLeft: "auto",
            flexWrap: "wrap",
            marginTop: "10px"

        },

    }))
;

interface InfoProps {
    path: string,
    text: string,
}

const NavigationBar: React.FC<InfoProps> = (props) => {
    const history = useHistory()
    return (

        <div className={useStyles().navigationBar}>
            <Button variant="outlined" onClick={() => history.push(props.path)}>{props.text}</Button>
            <Button variant="outlined">Om oss</Button>
        </div>
    );
};


export default NavigationBar;
