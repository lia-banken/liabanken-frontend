import {Button, makeStyles} from '@material-ui/core';
import React from 'react';
import {useHistory} from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
    tabs: {
        display: "flex",
        flexWrap:"wrap",
        minWidth: "10%",
        marginTop: "20px",
    }
}))
const SelectionButtons: React.FC = props => {
    const history = useHistory()
    return (
        <div>
            <div className={useStyles().tabs}>
                <Button variant="outlined" onClick={() => history.push("/admin/mina-sidor")}>Mina sidor</Button>
                <Button variant="outlined" onClick={() => history.push("/admin/skolor")}>Skolor</Button>
                <Button variant="outlined" onClick={() => history.push("/admin/arbetsgivare")}> Arbetsgivare</Button>
                <Button variant="outlined" onClick={() => history.push("/admin/admins")}>Admin</Button>
            </div>
        </div>
    );
};


export default SelectionButtons;
