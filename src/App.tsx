import React from 'react';
import LandingPage from './pages/LandingPage';
import Footer from "./components/Footer";
import {BrowserRouter, Switch, Route} from 'react-router-dom';

import Header from './components/Header';
import MyPages from './pages/MyPages';
import Schools from './pages/Schools';
import Companies from './pages/Companies';
import Admins from './pages/Admins';
import {makeStyles} from "@material-ui/core";

// const useStyles = makeStyles(() => ({
//     root: {
//         margin: "0px",
//         border:"2px solid red"
//     },
//     footer:{
//         margin: "0px",
//         top:"10px"
//     }
// }))

function App() {
    return (

        <BrowserRouter>
            <Header/>
            <Switch>
                <Route path="/admin/admins" component={Admins}/>
                <Route path="/admin/arbetsgivare" component={Companies}/>
                <Route path="/admin/skolor" component={Schools}/>
                <Route path="/admin/mina-sidor" component={MyPages}/>
                <Route path="/" component={LandingPage}/>
            </Switch>
            <Footer/>
        </BrowserRouter>


    );
}

export default App;
