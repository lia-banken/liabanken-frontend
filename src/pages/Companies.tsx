import {Button, makeStyles} from '@material-ui/core';
import React, {useState} from 'react';
import {useHistory} from 'react-router-dom';
import NavigationBar from '../components/NavigationBar';
import SelectionButtons from '../components/SelectionButtons';
import SearchBar from "../components/SearchBar";
import Btn from "../components/Btn";
import Dropdown from "../components/Dropdown";
import ListItems from "../components/ListItems";
import QueryList from "../components/QueryList";

const useStyles = makeStyles((theme) => ({
    selectionButtons: {
        display: "flex",
        justifyContent: "center",
        marginLeft: "auto",
        marginRight: "auto"
    },
    searchBar: {
        display: 'flex',
        justifyContent: 'center',
    },

    schoolListandSchoolButtonContainer: {
        display: "flex",
        flexDirection: "row",
    },
    schoolButtons: {
        display: "flex",
        flexDirection: "column",
        marginRight: "15%"
    },
}))
const Companies: React.FC = props => {
    const classes = useStyles();
    const history = useHistory()
    const [searchText, setSearchText] = useState<string>("");
    const [city, setCity] = useState<string>("");
    const [compnayListShow, setcompnayListShow] = useState(true);
    const [companyListQuery, setCompanyListQuery] = useState(false);
    const [companiesList, setCompaniesList] = useState([
        {id: '1', name: 'Nexter', ort: 'stockholm'},
        {id: '2', name: 'Sigma', ort: 'malmö'},
        {id: '3', name: 'Sensera', ort: 'örebro'},
        {id: '4', name: 'Yazans AB', ort: 'stockholm'},
    ]);
    const updateCity = (ort: string): any => {
        setCity(ort)
    }

    const navbarInfo = {
        path: "/",
        text: "Logga ut"
    };
    const updateSearchText = (searchTextFromChild: string): any => {
        setSearchText(searchTextFromChild)
    }
    const companyList = () => {
        return (
            <div>
                <div className={classes.searchBar}>
                    <SearchBar updateSearchText={updateSearchText}/>
                    <Dropdown updateCity={updateCity}/>
                </div>
                <ListItems city={city} searchText={searchText} list={companiesList}/>
            </div>)
    }

    const queryList = () => {
        return (
            <div>
                <QueryList/>
            </div>)
    }

    const toggelButton = (textFromClickedButton: string) => {
        switch (textFromClickedButton) {
            case "companyList":
                setcompnayListShow(true)
                setCompanyListQuery(false)
                break;
            case "queryList":
                setCompanyListQuery(true)
                setcompnayListShow(false)
                break;
            default:
                console.log("not working");
        }
    }

    return (
        <div>
            <NavigationBar path={navbarInfo.path} text={navbarInfo.text}/>
            <div className={classes.selectionButtons}>
            <SelectionButtons/>
            </div>

            <div className={classes.schoolListandSchoolButtonContainer}>
                <div className={classes.schoolButtons}>
                    <Button onClick={() => toggelButton("companyList")}>CompanyList</Button>
                    <Button onClick={() => toggelButton("queryList")}>Förfrågningar</Button>
                </div>
                {
                    (compnayListShow) ? companyList() : queryList()
                }
            </div>

        </div>
    );
};

export default Companies;
