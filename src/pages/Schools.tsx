import {Button, makeStyles} from '@material-ui/core';
import React, {Provider, useEffect, useState} from 'react';
import {useHistory} from 'react-router-dom';
import NavigationBar from '../components/NavigationBar';
import SelectionButtons from '../components/SelectionButtons';
import SearchBar from "../components/SearchBar";
import QueryList from "../components/QueryList";
import Dropdown from "../components/Dropdown";
import ListItems from "../components/ListItems";


const useStyles = makeStyles((theme) => ({
    selectionButtons: {
        display: "flex",
        justifyContent: "center",
        marginLeft: "auto",
        marginRight: "auto"
    },

    searchBar: {
        display: 'flex',
        justifyContent: 'center',
    },

    schoolListandSchoolButtonContainer: {
        display: "flex",
        flexDirection: "row",
    },
    schoolButtons: {
        display: "flex",
        flexDirection: "column",
        marginRight: "15%"
    },
}))

const Schools: React.FC = () => {
    const [clickedOnList, setClickedOnList] = useState(true);
    const [clickedOnQuery, setClickedOnQuery] = useState(false);

    const [schoolList, setSchoolList] = useState([
        {id: '1', name: 'EC-Utbildning', ort: 'stockholm'},
        {id: '2', name: 'KTH', ort: 'malmö'},
        {id: '3', name: 'Uppsala Universitet', ort: 'örebro'},
        {id: '4', name: 'Stockholms Universitet', ort: 'stockholm'},
    ]);
    const classes = useStyles();

    const [city, setCity] = useState<string>("");
    const [searchText, setSearchText] = useState<string>("");

    const updateCity = (ort: string): any => {
        setCity(ort)
    }

    const updateSearchText = (searchTextFromChild: string): any => {
        setSearchText(searchTextFromChild)
    }

    const navbarInfo = {
        path: "/",
        text: "Logga ut"
    };

    const skolLista = () => {
        return (
            <div>
                <div className={classes.searchBar}>
                    <SearchBar updateSearchText={updateSearchText}/>
                    <Dropdown updateCity={updateCity}/>
                </div>
                <ListItems city={city} searchText={searchText} list={schoolList}/>
            </div>)
    }

    const queryList = () => {
        return (
            <div>
                <QueryList/>
            </div>)
    }

    const toggelButton = (props: string) => {
        if (props === "queryList") {
            if (clickedOnList)
                setClickedOnList(false)
            setClickedOnQuery(true)
        } else if (props === "skolList") {
            if (clickedOnQuery)
                setClickedOnQuery(false)
            setClickedOnList(true)
        }
    }

    return (
        <div>
            <NavigationBar path={navbarInfo.path} text={navbarInfo.text}/>
            <div className={classes.selectionButtons}>
                <SelectionButtons/>
            </div>

            <div className={classes.schoolListandSchoolButtonContainer}>
                <div className={classes.schoolButtons}>
                    <Button onClick={() => toggelButton("skolList")}>Skollista</Button>
                    <Button onClick={() => toggelButton("queryList")}>Förfrågningar</Button>
                </div>
                {
                    clickedOnList && skolLista()
                }
                {
                    clickedOnQuery && queryList()
                }
            </div>
        </div>
    );
};

export default Schools;
