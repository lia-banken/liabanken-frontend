import {Button, makeStyles} from '@material-ui/core';
import React, {useState} from 'react';
import {useHistory} from 'react-router-dom';
import NavigationBar from '../components/NavigationBar';
import SelectionButtons from '../components/SelectionButtons';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({

    selectionButtons: {
        display: "flex",
        justifyContent:"center",
        marginLeft:"auto",
        marginRight:"auto"
    },

    myPagesBody: {
        display: "flex",
        flexDirection: "row"
    },

    myPageBodyButton: {
        display: "flex",
        flexDirection: "column",
        minWidth: "10%",
        marginRight:"20px"
    },

    myPagesForm: {
        '& > *': {
            margin: theme.spacing(3),
            width: '25ch',

        },
        display: "flex",
        margin: "20px",
        padding: "10px",
        width: "300px",
        justifyContent: "space-around",
        backgroundColor: "#D3F5F3",
        flexDirection: "column",
        alignItems: "center"
    },
    enterButton:{
        marginBottom: "60px"
    }

}))
const MyPages: React.FC = props => {
    const classes = useStyles();
    const [clicked, setClicked] = useState(false);
    const [changePasswordClicked, setChangePassWordClicked] = useState(false);
    const [adminData, setAdminData] = useState([{
        firstname: "mine",
        secondName:"Jansson",
        telofonnumber: "0934343234",
        address: "stockholm",
        password: "*******",
        email:"mineJansoo@gmail.com",
    }]);
    const navbarInfo = {
        path: "/",
        text: "Logga ut"
    };
    const toggleMyPageButton = (props: string) => {

        if (props === "1") {
            if (clicked)
                setClicked(false);
            setChangePassWordClicked(!changePasswordClicked)
        }
        if (props === "2") {
            if (changePasswordClicked)
                setChangePassWordClicked(false);
            setClicked(!clicked)
        }
    }
    const changeProfileButton = () => {
        return (
            <div>
            <form className={classes.myPagesForm} noValidate autoComplete="off">
                <TextField id="standard-basic" label="Förnamn"/>
                <TextField id="standard-basic" label="Efternamn"/>
                <TextField id="standard-basic" label="Email"/>
                <TextField id="standard-basic" label="Telefonnummer"/>
                <TextField id="standard-basic" label="Adress"/>
            </form>
                <Button className={classes.enterButton} variant="outlined" >Enter</Button>
            </div>

        )
    }
    const changePasswordButton = () => {
        return (
            <div>
            <form className={classes.myPagesForm} noValidate autoComplete="off">
                <TextField id="standard-basic" label="Gammalt lösenord"/>
                <TextField id="standard-basic" label="Nytt lösenord"/>
                <TextField id="standard-basic" label="Bekkräfta lösenord"/>
            </form>
        <Button variant="outlined" >Enter</Button>
    </div>
        )
    }

    const adminInfo = () => {
        return (
            <div className={classes.myPagesForm}>
                <p>{adminData[0].firstname}</p>
                <p>{adminData[0].secondName}</p>
                <p>{adminData[0].email}</p>
                <p>{adminData[0].address}</p>
                <p>{adminData[0].telofonnumber}</p>
            </div>
        )
    }
    return (
            <div>
                <NavigationBar path={navbarInfo.path} text={navbarInfo.text}/>
                <div className={classes.selectionButtons}>
                    <SelectionButtons/>
                </div>

                <div className={classes.myPagesBody}>
                    <div className={classes.myPageBodyButton}>
                        <p>MINA SIDOR</p>
                        <Button variant="outlined" onClick={() => toggleMyPageButton("2")}>Redigera profil</Button>
                        <Button variant="outlined" onClick={() => toggleMyPageButton("1")}>Ändra lösenord</Button>
                    </div>
                    {
                        (!clicked && !changePasswordClicked) && adminInfo()
                    }

                    {clicked &&  changeProfileButton()}
                    {changePasswordClicked && changePasswordButton()}
                </div>
            </div>
    );
};


export default MyPages;
