import {Button, makeStyles} from '@material-ui/core';
import React from 'react';
import {useHistory} from 'react-router-dom';
import NavigationBar from '../components/NavigationBar';
import SelectionButtons from '../components/SelectionButtons';
import AddIcon from '@material-ui/icons/AddCircleOutline';
import ReusableList, {IDataList} from "../components/ReusableList";
import {AdminCreateAccountModal} from "../components/AdminCreateAccountModal";

const useStyles = makeStyles((theme) => ({
    selectionButtons: {
        display: "flex",
        justifyContent: "center",
        marginLeft: "auto",
        marginRight: "auto"
    },
    adminListandAddAdminButtonContainer: {
        display: "flex",
        flexDirection: "row",
    },
    addAdminButton: {
        display: "flex",
        flexDirection: "column",
        marginRight: "15%"
    },
}))
const Admins: React.FC = props => {
    const classes = useStyles();
    const history = useHistory()

    const [adminList, setAdminList] = React.useState<IDataList[]>([
        {
            name: "Agnes", lastname: "Gna", email: "ddf", password: "f"
        },
        {
             name: "Yazan", lastname: "Alkh", email: "ddff", password: "f"
        },
        {
            name: "Denise", lastname: "Lein", email: "ddff", password: "f"
        },
        {
            name: "Kristoffer", lastname: "Pett", email: "ddfff", password: "f"
        },
        {
             name: "Rinat", lastname: "Lunu", email: "dddddd", password: "f"
        },
        {
           name: "Ermiase", lastname: "Jir", email: "ddfbdb", password: "f"
        },
    ]);

    const navbarInfo = {
        path: "/",
        text: "Logga ut"
    };

    const addItem = (name:string, lastName:string,email:string) => {
        const newItem : IDataList = {name:name, lastname:lastName,email:email};
        setAdminList( (prev: IDataList[]) : IDataList[] => [...prev, newItem])

    }

    const deliteAdminById = (email: string) :void => {
        setAdminList(adminList.filter((removeItem) => removeItem.email !== email))
    }

    return (
        <div>
            <NavigationBar path={navbarInfo.path} text={navbarInfo.text}/>
            <div className={classes.selectionButtons}>
                <SelectionButtons/>
            </div>
            <p>ADMINS</p>

            <div className={classes.adminListandAddAdminButtonContainer}>
                <div className={classes.addAdminButton}>
            <AdminCreateAccountModal onAdd={addItem}/>
                </div>
            <ReusableList list={adminList} remove={deliteAdminById}/>
            </div>
        </div>
    );
};


export default Admins;
