import React, {useState} from 'react';
import Divider from '@material-ui/core/Divider';
import {makeStyles} from '@material-ui/core';
import CreateAccountModal from "../components/CreateAccountModal";
import NavigationBar from '../components/NavigationBar';

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex",
        flexDirection: "column",
        margin: 0,
        padding: 0
    },
    navigationBar: {
        display: "flex",
        justifyContent: "space-between",

        flexDirection: "column",
        marginLeft: "30px",
        marginRight: "30px"
    },
    textContainer: {
        display: "flex",
        flexDirection: "column",
        textAlign: "center",
        padding: "10px",
        paddingTop: "10px",
        paddingBottom: "20px",
        marginRight: "50px",
        marginLeft: "50px",
    }

}))

const LandingPage: React.FC = () => {
    const [text, setText] = useState("Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae doloremque est hic praesentium quibusdam. Accusamus adipisci assumenda delectus, fugiat inventore minima nemo quos tempore. Iure maiores officiis quasi qui sit? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae doloremque est hic praesentium quibusdam. Accusamus adipisci assumenda delectus, fugiat inventore minima nemo quos tempore. Iure maiores officiis quasi qui sit? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae doloremque est hic praesentium quibusdam. Accusamus adipisci assumenda delectus, fugiat inventore minima nemo quos tempore. Iure maiores officiis quasi qui sit?");
    const navbarInfo = {
        path: "/admin/mina-sidor",
        text: "Logga in"
    };


    return (
        <div className={useStyles().root}>
            <div className={useStyles().navigationBar}>
                <NavigationBar path={navbarInfo.path} text={navbarInfo.text}/>
            </div>
            <div className={useStyles().textContainer}>
                <p>{text}</p>
            </div>
            <Divider/>
            <CreateAccountModal/>
        </div>
    );
};

export default LandingPage;
